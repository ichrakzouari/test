package com.example.test;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @RequestMapping
    public String helloWord() {
        return "hello world from SpringBoot";
    }

    @RequestMapping("/goodbye")
    public String goodbye() {
        return "goodbye from SpringBoot";
    }
}
